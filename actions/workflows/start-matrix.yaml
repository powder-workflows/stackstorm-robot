version: 1.0

description: "Run a test series on the Celona gear, starting the SUT and Robot experiments"

input:
  - proj
  - username
  - robot_profile_name
  - robot_expt_name
  - sut_profile_name
  - sut_expt_name
  - manager_privkey
  - manager_pubkey
  - max_attenuation
  - attenuation_step
  - celona_version
  - skip_tcms
  - hold_experiments

vars:
  - robot_node: null
  - robot_expt_status: null
  - robot_terminate: false
  - sut_terminate: false
  - upload_results: null
  - download_results: null
  - ping_results: null
  - sut_client: "nuc22"
  - sut_server: "pc797"

tasks:

  #
  # For faster debugging, We can leave these experiments running.
  #
  start_experiments:
    action: core.noop
    next:
      - do:
          - maybe_start_robot_expt
          - maybe_start_sut_expt

  maybe_start_sut_expt:
    action: emulab.experiment.status name="<% ctx().sut_expt_name %>" proj="<% ctx().proj %>" max_wait_time=5 errors_are_fatal=False wait_for_status=False
    next:
      - when: <% succeeded() and result().result.result_code = 0 %>
        publish:
          - sut_terminate: false
        do:
          - start_complete
      - when: <% succeeded() and result().result.result_code != 0 %>
        do:
          - start_sut_expt
      - when: <% failed() %>
        do:
          - test_failure

  start_sut_expt:
    action: emulab.experiment.create name="<% ctx().sut_expt_name %>" proj="<% ctx().proj %>" profile="<% ctx().sut_profile_name %>" wait_for_status=True sshpubkey='<% ctx().manager_pubkey %>'
    next:
      - when: <% succeeded() %>
        publish:
          - sut_terminate: true
        do:
          - start_complete
      - when: <% failed() %>
        do:
          - test_failure

  maybe_start_robot_expt:
    action: emulab.experiment.status name="<% ctx().robot_expt_name %>" proj="<% ctx().proj %>" max_wait_time=5 errors_are_fatal=False wait_for_status=False
    next:
      - when: <% succeeded() and result().result.result_code = 0 %>
        publish:
          - robot_terminate: false
          - robot_node: robot.<% ctx().robot_expt_name %>.<% ctx().proj + '.emulab.net' %>
        do:
          - start_complete
      - when: <% succeeded() and result().result.result_code != 0 %>
        do:
          - start_robot_expt
      - when: <% failed() %>
        do:
          - test_failure

  start_robot_expt:
    action: emulab.experiment.create name="<% ctx().robot_expt_name %>" proj="<% ctx().proj %>" profile="<% ctx().robot_profile_name %>" wait_for_status=True sshpubkey='<% ctx().manager_pubkey %>'
    next:
      - when: <% succeeded() %>
        publish:
          - robot_terminate: true
          - robot_expt_status: <% result().result %>
          - robot_node: robot.<% ctx().robot_expt_name %>.<% ctx().proj + '.emulab.net' %>
        do:
          - setup_robot
      - when: <% failed() %>
        do:
          - test_failure

  setup_robot:
    action: core.remote hosts=<% ctx(robot_node) %> username='<% ctx().username %>' private_key='<% ctx().manager_privkey %>' cmd='cd /var/tmp; git clone https://gitlab.flux.utah.edu/stoller/robot-iperf.git' timeout=60
    next:
      - when: <% succeeded() %>
        do:
          - start_complete
      - when: <% failed() %>
        do:
          - test_failure

  start_complete:
    join: 2
    action: core.noop
    next:
      - do:
          - set_sut_names

  set_sut_names:
    action: core.noop
    next:
      - when: <% succeeded() %>
        publish:
          - sut_client: ue1.<% ctx().sut_expt_name %>.<% ctx().proj + '.emulab.net' %>
          - sut_server: proxy.<% ctx().sut_expt_name %>.<% ctx().proj + '.emulab.net' %>
        do:
          - setup_sut_sshkey_on_server
      - when: <% failed() %>
        do:
          - test_failure

  setup_sut_sshkey_on_server:
    action: core.remote hosts=<% ctx().sut_server %> username='<% ctx().username %>' private_key='<% ctx().manager_privkey %>' cmd="geni-get key > /tmp/key.pem; chmod 600 /tmp/key.pem; ssh-keygen -y -f /tmp/key.pem >> /users/<% ctx().username %>/.ssh/authorized_keys2" timeout=60
    next:
      - when: <% succeeded() %>
        do:
          - setup_sut_sshkey_on_client
      - when: <% failed() %>
        do:
          - test_failure

  setup_sut_sshkey_on_client:
    action: core.remote hosts=<% ctx().sut_client %> username='<% ctx().username %>' private_key='<% ctx().manager_privkey %>' cmd="geni-get key > /tmp/key.pem; chmod 600 /tmp/key.pem; ssh-keygen -y -f /tmp/key.pem >> /users/<% ctx().username %>/.ssh/authorized_keys2" timeout=60
    next:
      - when: <% succeeded() %>
        do:
          - setup_sshkey_on_robot
      - when: <% failed() %>
        do:
          - test_failure

  setup_sshkey_on_robot:
    action: core.remote hosts=<% ctx(robot_node) %> username='<% ctx().username %>' private_key='<% ctx().manager_privkey %>' cmd='/local/repository/stackstorm-setup.pl <% ctx().proj %>,<% ctx().sut_expt_name %>' timeout=60
    next:
      - when: <% succeeded() %>
        do:
          - start_matrix_tests
      - when: <% failed() %>
        do:
          - test_failure

  start_matrix_tests:
    action: robot.start-matrix-tests proj="<% ctx().proj %>" username="<% ctx().username %>" robot_expt_name="<% ctx().robot_expt_name %>" sut_expt_name="<% ctx().sut_expt_name %>" manager_privkey="<% ctx().manager_privkey %>" manager_pubkey="<% ctx().manager_pubkey %>" celona_version="<% ctx().celona_version %>" skip_tcms="<% ctx().skip_tcms %>" max_attenuation="<% ctx().max_attenuation %>" attenuation_step="<% ctx().attenuation_step %>"
    next:
      - do:
          - maybe_terminate_experiments

  maybe_terminate_experiments:
    action: core.noop
    next:
      - when: <% not ctx(hold_experiments) %>
        do:
          - terminate_experiments

  terminate_experiments:
    action: core.noop
    next:
      - do:
          - maybe_terminate_robot_experiment
          - maybe_terminate_sut_experiment

  maybe_terminate_robot_experiment:
    action: core.noop
    next:
      - when: <% ctx().robot_terminate %>
        do:
          - terminate_robot_experiment
      - when: <% not ctx().robot_terminate %>
        do:
          - terminations_complete

  terminate_robot_experiment:
    action: emulab.experiment.terminate name='<% ctx().robot_expt_name %>' proj='<% ctx().proj %>'
    next:
      - do:
          - terminations_complete

  maybe_terminate_sut_experiment:
    action: core.noop
    next:
      - when: <% ctx().sut_terminate %>
        do:
          - terminate_sut_experiment
      - when: <% not ctx().sut_terminate %>
        do:
          - terminations_complete

  terminate_sut_experiment:
    action: emulab.experiment.terminate name='<% ctx().sut_expt_name %>' proj='<% ctx().proj %>'
    next:
      - do:
          - terminations_complete

  terminations_complete:
    join: 2
    action: core.noop

  test_failure:
    action: core.noop
    
